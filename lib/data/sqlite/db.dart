import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBInit {
  static const databaseName = 'task_db.db';

  Future<Database> initDatabase() async {
    print(join(await getDatabasesPath()));
    return await openDatabase(join(await getDatabasesPath(), databaseName),
        version: 2, onCreate: this._create);
  }

  Future _create(Database db, int version) async {
    await db.execute(
        '''CREATE TABLE task_status (id INTEGER PRIMARY KEY, name TEXT NOT NULL)''');

    await db.execute('''CREATE TABLE tasks (
              id INTEGER PRIMARY KEY, 
              task_status_id INTEGER NOT NULL,
              name TEXT NOT NULL,
              status INTEGER NOT NULL,
              FOREIGN KEY (task_status_id) REFERENCES task_status (id) 
                ON DELETE NO ACTION ON UPDATE NO ACTION
            )''');
    return;
  }
}
