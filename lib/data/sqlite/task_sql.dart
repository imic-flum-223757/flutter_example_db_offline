import 'package:flutter_example_db_offline/model/task.dart';
import 'package:sqflite/sqflite.dart';

import 'db.dart';
import 'task_status_sql.dart';

class TaskSql {
  static Database _database;
  static DBInit _initDb = DBInit();
  static TaskSSql _taskSSql;

  Future<Database> get database async {
    if (_database == null) {
      return await _initDb.initDatabase();
    }
    return _database;
  }

  Future<List<Task>> fetchAllTask() async {
    var db = await database;
    var taskData = await db.query('tasks');
    return taskData.map((e) => Task.fromJson(e)).toList();
  }

  Future<Task> addTask(Task taskData) async {
    Database db = await database;
    int taskId = await db.insert('tasks', taskData.toMap());
    print('insert_id $taskId');
    taskData.id = taskId;
    return taskData;
  }

  Future<Task> updateTask(Task taskData) async {
    Database db = await database;
    await db.update('tasks', taskData.toMap(),
        where: 'id = ?', whereArgs: [taskData.id]);
    return taskData;
  }

  Future<void> deleteTask(Task taskData) async {
    Database db = await database;
    await db.delete('tasks', where: 'id = ?', whereArgs: [taskData.id]);
  }

  Future<Task> fetchTaskStatus(int taskId) async {
    Database db = await database;
    List<Map> results =
        await db.query("tasks", where: "id = ?", whereArgs: [taskId]);
    Task task = Task.fromJson(results[0]);
    task.taskStatus = await _taskSSql.fetchStatusByID(task.taskStatusId);
    return task;
  }

  Future<List<Task>> fetchTaskStatusRaw() async {
    Database db = await database;
    List<Map> results = await db.rawQuery('''
      SELECT t.id, t.name,t.status,t.task_status_id, st.id as sid, st.name as sName 
      FROM "tasks" t
      INNER JOIN task_status as st 
      ON st.id = t.task_status_id;
    ''');
    print('data Raw: $results');
    return results.map((e) => Task.fromJson(e)).toList();
  }

  Future<Task> fetchTaskStatusRawWithID(int taskId) async {
    Database db = await database;
    List<Map> results =
        await db.rawQuery('''SELECT * FROM "tasks" WHERE id = $taskId''');
    print(results);
    Task task = Task.fromJson(results[0]);
    return task;
  }
}
