import 'package:flutter_example_db_offline/model/task.dart';
import 'package:sqflite/sqflite.dart';

import 'db.dart';

class TaskSSql {
  static Database _database;
  static DBInit _initDb = DBInit();

  Future<Database> get database async {
    if (_database == null) {
      return await _initDb.initDatabase();
    }
    return _database;
  }

  Future<List<TaskStatus>> fetch() async {
    var db = await database;
    var taskData = await db.query('task_status');
    return taskData.map((e) => TaskStatus.fromJson(e)).toList();
  }

  Future<TaskStatus> add(TaskStatus taskSData) async {
    Database db = await database;
    int taskId = await db.insert('task_status', taskSData.toMap());
    taskSData.id = taskId;
    return taskSData;
  }

  Future<TaskStatus> update(TaskStatus taskSData) async {
    Database db = await database;
    await db.update('task_status', taskSData.toMap(),
        where: 'id = ?', whereArgs: [taskSData.id]);
    return taskSData;
  }

  Future<void> delete(Task taskSData) async {
    Database db = await database;
    await db.delete('task_status', where: 'id = ?', whereArgs: [taskSData.id]);
  }

  Future<TaskStatus> fetchStatusByID(int id) async {
    Database db = await database;
    List<Map> results =
        await db.query("task_status", where: "id = ?", whereArgs: [id]);

    TaskStatus taskStatus = TaskStatus.fromJson(results[0]);

    return taskStatus;
  }
}
