class TaskStatus {
  int id;
  String name;

  TaskStatus({this.id, this.name});

  factory TaskStatus.fromJson(Map<String, dynamic> json) {
    return TaskStatus(id: json['id'], name: json['name']);
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'name': name};
  }
}

class Task {
  int id;
  String name;
  bool status; // 0, 1
  int taskStatusId;
  TaskStatus taskStatus;

  Task(
      {this.id,
      this.name,
      this.status = false,
      this.taskStatus,
      this.taskStatusId = 0});

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
        id: json['id'],
        name: json['name'],
        status: json['status'] == 0 ? false : true,
        taskStatusId: json['task_status_id'],
        taskStatus: json['sid'] != null && json['sName'] != null
            ? TaskStatus(id: json['sid'], name: json['sName'])
            : null);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'status': status ? 1 : 0,
      'task_status_id': taskStatusId
    };
  }
}
