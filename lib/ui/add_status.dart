import 'package:flutter/material.dart';
import 'package:flutter_example_db_offline/model/task.dart';
import 'package:flutter_example_db_offline/view_models/sqlLite_view_mode.dart';

class TaskStatusScreen extends StatefulWidget {
  final SqliteViewModel model;
  final TaskStatus taskStatus;

  const TaskStatusScreen({Key key, @required this.model, this.taskStatus})
      : super(key: key);

  @override
  _TaskStatusScreenState createState() => _TaskStatusScreenState();
}

class _TaskStatusScreenState extends State<TaskStatusScreen> {
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.taskStatus != null) {
      controller.text = widget.taskStatus.name;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ADD Task'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              controller: controller,
              decoration: InputDecoration(hintText: 'Your custom status'),
              autocorrect: false,
            ),
            RaisedButton(
              onPressed: () async {
                if (widget.taskStatus != null) {
                  return;
                }
                await widget.model
                    .addStatusTask(TaskStatus(name: controller.text));
                Navigator.of(context).pop();
              },
              child: Text('Save Task'),
            )
          ],
        ),
      ),
    );
  }
}
