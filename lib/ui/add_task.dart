import 'package:flutter/material.dart';
import 'package:flutter_example_db_offline/model/task.dart';
import 'package:flutter_example_db_offline/view_models/sqlLite_view_mode.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddTaskScreen extends StatefulWidget {
  final SqliteViewModel model;
  final Task task;

  const AddTaskScreen({Key key, @required this.model, this.task})
      : super(key: key);

  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  TextEditingController controller = TextEditingController();
  int dropdownValue = 0;
  List<TaskStatus> statuses = [];
  FToast fToast;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.task != null) {
      controller.text = widget.task.name;
      if(widget.task.taskStatusId != null){
        dropdownValue = widget.task.taskStatusId;
      }
    }
    if (widget.model.tasksStatuses.length > 0) {
      statuses = [
        TaskStatus(id: 0, name: 'Select status'),
        ...widget.model.tasksStatuses
      ];
    }
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ADD Task'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButton<int>(
                value: dropdownValue,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: statuses.map<DropdownMenuItem<int>>((val) {
                  return DropdownMenuItem<int>(
                    value: val.id,
                    child: Text(val.name),
                  );
                }).toList(),
              ),
              TextField(
                controller: controller,
                decoration: InputDecoration(hintText: 'Nhap vao cong viec'),
                autocorrect: false,
              ),
              RaisedButton(
                onPressed: () async {
                  if (widget.task != null) {
                    await widget.model.updateTask(Task(
                        name: controller.text,
                        status: widget.task.status,
                        id: widget.task.id));
                  } else {
                    if (dropdownValue == 0 || controller.text.isEmpty) {
                      this._showToast();
                      return;
                    }
                    await widget.model.addTask(Task(
                        name: controller.text, taskStatusId: dropdownValue));
                  }

                  Navigator.of(context).pop();
                },
                child: Text('Save Task'),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showToast() {
    Widget toast = Container(
      padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Validate error"),
        ],
      ),
    );
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }
}
