import 'package:flutter/material.dart';
import 'package:flutter_example_db_offline/base/provider_view.dart';
import 'package:flutter_example_db_offline/model/task.dart';
import 'package:flutter_example_db_offline/ui/add_task.dart';
import 'package:flutter_example_db_offline/view_models/sqlLite_view_mode.dart';

import 'add_status.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ProviderView<SqliteViewModel>(
        model: SqliteViewModel(),
        onReady: (model) => model.fetchData(),
        builder: (_, model, __) {
          return Scaffold(
              appBar: AppBar(
                title: Text(widget.title),
                actions: [
                  IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddTaskScreen(
                                      model: model,
                                    )));
                      })
                ],
              ),
              body: _bodyBuilder(model));
        });
  }

  Widget _bodyBuilder(SqliteViewModel model) {
    if (model.isLoading) {
      return _loading();
    }
    if (!model.isLoading && model.tasks.length > 0) {
      return ListView.builder(
        itemBuilder: (context, index) =>
            _buildListItem(model.tasks[index], model),
        itemCount: model.tasks.length,
      );
    }
    return _noData(model);
  }

  Widget _buildListItem(Task task, SqliteViewModel model) {
    return ListTile(
      title: Text('${task.name}'),
      subtitle: Text('${task.taskStatus != null ? task.taskStatus.name : ''}'),
      trailing: IconButton(
        onPressed: () {
          model.deleteTask(task);
        },
        icon: Icon(Icons.delete),
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AddTaskScreen(
                      model: model,
                      task: task,
                    )));
      },
    );
  }

  Widget _noData(SqliteViewModel model) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('No data found.'),
          SizedBox(
            height: 10,
          ),
          FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.add), Text('Create Status Task')],
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TaskStatusScreen(model: model)));
              }),
          FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.add), Text('Create Task')],
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddTaskScreen(model: model)));
              })
        ],
      ),
    );
  }

  Widget _loading() {
    return Center(
      child: Text('Load du lieu.'),
    );
  }
}
