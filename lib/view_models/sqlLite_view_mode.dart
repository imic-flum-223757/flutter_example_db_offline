import 'package:flutter/cupertino.dart';
import 'package:flutter_example_db_offline/data/sqlite/task_sql.dart';
import 'package:flutter_example_db_offline/data/sqlite/task_status_sql.dart';
import 'package:flutter_example_db_offline/model/task.dart';

class SqliteViewModel extends ChangeNotifier {
  TaskSql _taskSql = TaskSql();
  TaskSSql _taskSSql = TaskSSql();
  List<Task> _tasks = [];
  List<TaskStatus> _taskStatuses = [];
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  List<Task> get tasks => _tasks;

  List<TaskStatus> get tasksStatuses => _taskStatuses;

  Future<void> fetchData() async {
    _isLoading = true;
    notifyListeners();
    _tasks = await _taskSql.fetchTaskStatusRaw();
    _taskStatuses = await _taskSSql.fetch();
    _isLoading = false;
    notifyListeners();
  }

  Future<void> addTask(Task taskData) async {
    Task newTask = await _taskSql.addTask(taskData);
    _tasks.add(newTask);
    print(_tasks.length);
    notifyListeners();
  }

  Future<void> updateTask(Task taskData) async {
    await _taskSql.updateTask(taskData);
    int index = _tasks.indexWhere((Task task) => task.id == taskData.id);
    if (index >= 0) {
      _tasks[index] = taskData;
    }
    notifyListeners();
  }

  Future<void> deleteTask(Task taskData) async {
    await _taskSql.deleteTask(taskData);
    _tasks.removeWhere((Task task) => task.id == taskData.id);
  }

  Future<void> addStatusTask(TaskStatus taskStatus) async {
    TaskStatus newSTask = await _taskSSql.add(taskStatus);
    _taskStatuses.add(newSTask);
    notifyListeners();
  }
}
